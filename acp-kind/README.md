<p align="center">
<img src="./ake.png" alt="ake" title="ake" width=350 />
</p>

![supported-platform](https://img.shields.io/badge/platform-linux%20%7C%20mac-blue?style=flat-square)
[![team](https://img.shields.io/badge/Channel-Makefile.ake-blue?style=flat-square&logo=microsoft-teams)](https://teams.microsoft.com/l/channel/19%3aea9ff6f81f1041c59218689ac2ea3022%40thread.skype/miniake?groupId=80e533e6-3ead-4ee9-a5b3-06423e12f213&tenantId=7ec61fd3-4825-4cf3-931a-f67c1156f955)

Makefile.ake is a Makefile supplement pack, it takes care of setting up acp/devops/asm/aml environment that you can use in your Makefiles. With Makefile.ake you can setting up alauda platforms anywhere with almost zero dependency. 
> The idea of positioning miniake as a Makefile supplement pack is borrowed from project [Makefile.venv](https://github.com/sio/Makefile.venv).

## Features
* Simple: you can setup acp with just `make acp` command.
* Idempotent: all targets in Makefile.ake are idempotent, you can execute target mulitple times with the same result.
* Clean: Makefile.ake touch nothing outside the `.miniake` directory.

## Prequirements
* kind>0.6
* helm~=2
* kubectl

## Installation
Here are two installation methods, you can choose any one according to your preference.
### Method1
Copy Makefile.ake to your project directory and add include statement to the bottom of your Makefile:
```
include Makefile.ake
```
### Method2
Clone this project as a submodule:
```
git submodule add git@bitbucket.org:mathildetech/toolkit.git
```
Then add include statement to the bottom of your Makefile:
```
include toolkit/acp-kind/Makefile.ake
```

## Usage
Makefile.ake provides the following targets to help you setup alauda platforms:

* `make acp`: Setup a k8s cluster using kind and install acp inside it. Multiple executions make the same result, feel free to run it.
* `make devops`: Same as target `acp` just for devops platform.
* `make asm`: Same as target `acp` just for asm platform.
* `make global`: Setup a k8s cluster using kind and only install helm, cert-manager, dex and alauda-base inside it. Run this target multiple times only create one cluster. This target is a dependency for targets `acp/devops/asm`.
* `make business`: Setup a k8s cluster and only install helm, cert-manager inside it. This cluster is used as a business cluster for alauda platform.
* `make join-global`: Join Global Cluster into ACP Platform

## Variables
Makefile.ake can be configured via following variables. These variables can be set as environment vars. But the recommended method is writing these variables in `miniake.env` file, Makefile.ake will automatically source the `miniake.env` file.    

* `IP`: the ip of your current computer.
* `CONSOLE_HTTPS_PORT`: the port on your computer allocated for acp https port, default is `443`.
* `CONSOLE_HTTP_PORT`: the port on your computer allocated for acp http port, default is `80`.
* `CLUSTER_IMAGE`: the image used for kind to setup the cluster, default is `index.alauda.cn/claas/kind-node:v1.15.3`
* `ALAUDA_CHART_ADDR`: the chart address used to fetch alauda platforms' charts, default is `http://7DO9QoLtDx1g:2wb4E1iydRj7@alauda-chart-release-new.ace-default.cloud.myalauda.cn/`.
* `ALAUDA_REGISTRY`: the docker registry used to download alauda platform's images, default is `index.alauda.cn`.
* `K8S_API_PORT`: the k8s api listen host port, default is `6443`

## Samples
Project cluster-registry's Makefile:
```Makefile
include Makefile.ake

deploy: manifests acp
	kubectl apply -f config/crd/bases
	kustomize build config/overlays/dev | kubectl apply -f -
```
Project cluster-registry's miniake.env:
```
IP=192.168.0.23
CONSOLE_HTTPS_PORT=8443
```
* Run `make deploy`, it will setup a acp platfrom locally and then deploy cluster-registry component inside it for debugging or testing.
* Open browser and enter `https://192.168.0.23:8443`, you'll see acp console is ready there.

## Known Issues
[ ] dex is only support CONSOLE_HTTPS_PORT = 443, other port will cause login problems.
    we could change dex configmap manually to fix this problems temporary.
[ ] we use pod ip of etcd in devops apiserver, so if etcd crashed, devops apiserver also crashed. 
  because there are some network problems when connect etcd cluster ip in devops apiserver that dose not be fixed.
[ ] cannot create project after platform started, there are some bugs when create project.
